    #!/usr/bin/python3
# -*- coding: utf-8 -*-

from commons import *
import jinja2
import json
import markdown
import os
import re
import urllib.request
from jinja2 import Template
latex_jinja_env = jinja2.Environment(
	block_start_string = '\BLOCK{',
	block_end_string = '}',
	variable_start_string = '\VAR{',
	variable_end_string = '}',
	comment_start_string = '\#{',
	comment_end_string = '}',
	line_statement_prefix = '%%',
	line_comment_prefix = '%#',
	trim_blocks = True,
	autoescape = False,
	loader = jinja2.FileSystemLoader(os.path.abspath('.'))
)

def convert_url(url):
    url = re.sub(r'https:\/\/www', r'https://api', url)
    url = re.sub(r'routes/([0-9]*)/.*', r'routes/\1?l=fr', url)
    return url

def get_route_json(url):
    data = urllib.request.urlopen(url).read()
    return json.loads(data.decode())

def extract_description(jsondata):
    descr_block = jsondata['locales'][0]
    description = {
        'title': descr_block['title_prefix'] + ' : ' + descr_block['title'],
        'description': descr_block['description'],
        'history': descr_block['route_history'],
        'gear': descr_block['gear'],
        'remarks': descr_block['remarks']
        }
    return description

def get_inline_images_ID(description):
    images = []
    for image in re.findall(r'\[img=([0-9]+)', description):
        images.append(image)
    return images

def get_image_infos(imagedata):
    infos = {
        'id': imagedata['document_id'],
        'filename': imagedata['filename'],
        'licence': licences[imagedata['image_type']],
        'title': imagedata['locales'][0]['title'],
        }
    if os.path.splitext(imagedata['filename'])[1] == '.svg':
        infos['filetype'] = 'SVG'
    else:
        infos['filetype'] = 'matriciel'
    return infos

def get_image_json(imageID):
    target = 'https://api.camptocamp.org/images/{}'.format(imageID)
    ret = urllib.request.urlopen(target).read()
    return json.loads(ret.decode())

def download_image(filename, image_id):
    name = str(image_id) + filename[-4:].lower()
    imgSource = 'https://media.camptocamp.org/c2corg_active/' + filename
    #if filename[-4:].lower() != '.svg'
    urllib.request.urlretrieve(imgSource, name)
    return name # On retourne le nom pour pouvoir le stocker

def md2LaTeX(texte):
    texte = markdown.markdown(texte)

    # Suppression des balises <p>
    texte = re.sub(r'<p>', '', texte)
    texte = re.sub(r'</p>', '', texte)

    # Les titres
    # Faut décaler d’un cran vers le haut
    texte = re.sub(r'<h2>(.*)</h2>', r'\\section{\1}', texte)
    texte = re.sub(r'<h3>(.*)</h3>', r'\\subsection{\1}', texte)
    
    # Les listes
    texte = re.sub(r'<ul>', r'\\begin{itemize}', texte)
    texte = re.sub(r'</ul>', r'\end{itemize}', texte)
    texte = re.sub(r'<li>(\n)?', r'\\item ', texte)
    texte = re.sub(r'</li>', '', texte)

    # La typo
    texte = re.sub(r'<em>(.*)</em>', r'\\emph{\1}', texte)

    # Caractères spéciaux
    texte = re.sub(r'&gt;', r'>', texte)
    texte = re.sub(r'&lt;', r'<', texte)

    # LaTeX tricks
    texte = re.sub(r'°', r'\\degree', texte)
    texte = re.sub(r'&', r'\\&', texte)

    # Les images
    texte = re.sub(r'\[img=(\d*)( \w*)\](.*)\[/img\]', r'''\\begin{figure}[hbtp]
    \\centering
    \\includegraphics[width=.8\\textwidth]{\1}
    \\caption{\3}
    \\end{figure}''', texte)

    # Les URL vers le topoguide
    texte = re.sub(r'\[\[(.*)\|(.*)\]\]', r'\2', texte)
    

    return texte
             
if __name__ == '__main__':
    
    url = 'https://www.camptocamp.org/routes/51460/fr/pic-n-des-grandes-lanches-grand-couloir-sw'
    url = convert_url(url)
    #dat = get_route_json(url)
    
    with open('sample.json', 'r') as dfile:
        data_raw = dfile.read()
    data_json = json.loads(data_raw)
    descr = extract_description(data_json)
    images = get_inline_images_ID(descr['description'])
    imagesList = []
    #for image in images:
        #imgJsn = get_image_json(image)
        #imgInfos = get_image_infos(imgJsn)
        #imgName = '{}.jpg'.format(imgInfos['id'])
        #imagesList.append(download_image(imgInfos['filename'], image))

    description = md2LaTeX(descr['description'])
    remarks = md2LaTeX(descr['remarks'])
    

#    with open('image.json', 'r') as ifile:
#        img_dat_raw = ifile.read()
#    img_json = json.loads(img_dat_raw)
#    infos = get_image_infos(img_json)
#    print(infos)

    print(images)
    #download_image(infos['filename'], infos['id'])
    template = latex_jinja_env.get_template('c2c.tex')
    print(template.render(title=descr['title'], description=description, remarks=remarks))
