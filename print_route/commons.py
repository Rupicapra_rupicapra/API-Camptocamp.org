#!/usr/bin/python3
# -*- coding: utf-8 -*-

activites = {
    'rock_climbing': 'Escalade',
    'hiking': 'Randonnée pédestre',
    'mountain_climbing': 'Rocher haute montagne',
    'skitouring': 'Ski de randonnée',
    'snow_ice_mixed': 'Neige, Glace et Mixte',
    'snowshoeing': 'Raquettes',
    'ice_climbing': 'Cascade de glace'
}

cotationOrder = ['engagement_rating',
                 'rock_free_rating',
                 'rock_required_rating',
                 'exposition_rock_rating',
                 'mixed_rating',
                 'ice_rating',
                 'equipment_rating',
                 'hiking_rating',
                 'snowshoe_rating',
                 'labande_global_rating',
                 'labande_ski_rating'
                 ]

eltsDescrBlock = ['title',
                  'description',
                  'history',
                  'gear',
                  'remarks',
                  ]

licences = {
    'collaborative': 'CC-By-SA'
    }
