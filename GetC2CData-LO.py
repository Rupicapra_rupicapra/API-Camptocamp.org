#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug  5 15:36:12 2017

@author: jerry
"""

activites = {
    'rock_climbing': 'Escalade',
    'hiking': 'Randonnée pédestre',
    'mountain_climbing': 'Rocher haute montagne',
    'skitouring': 'Ski de randonnée',
    'snow_ice_mixed': 'Neige, Glace et Mixte',
    'snowshoeing': 'Raquettes',
    'ice_climbing': 'Cascade de glace'
}

cotationOrder = ['engagement_rating',
                 'rock_free_rating',
                 'rock_required_rating',
                 'exposition_rock_rating',
                 'mixed_rating',
                 'ice_rating',
                 'equipment_rating',
                 'hiking_rating',
                 'snowshoe_rating',
                 'labande_global_rating',
                 'labande_ski_rating'
                 ]


limitElts = 120

# Also formats orientation… Anyway!
def get_orientation(document):
    orientations = document['orientations']
    return(orientations)

def format_orientation(orientationsList):
    if orientationsList is not None:
        return _format_orientation(orientationsList)
    else:
        return None

def _format_orientation(orientationsList):
    output = ''
    i = 1
    nbOrient = len(orientationsList)
    for orientation in orientationsList:
        output += orientation
        if i < nbOrient:
            output += '/'
        i += 1
    return output

def get_diffHeight(document):
    return(document['height_diff_difficulties'])

def get_cotation(document):
    cotation = {}
    for rating in cotationOrder:
        if rating in document:
            cotation[rating] = document[rating]
    return cotation

def _format_cotation(cotationDict):
    cleanCotation = {}
    for key, cotation in cotationDict.items():
        if cotation is not None:
            cleanCotation[key] = cotation
    
    nbCots = len(cleanCotation)
    currentCot = 1
    output = ""
    for cotation in cotationOrder:
        if cotation in cleanCotation:
            output += cleanCotation[cotation]
            
            if cotation == "rock_free_rating" and "rock_required_rating" in cleanCotation:
                output += " > "
            elif cotation == 'hiking_rating' and ('labande_global_rating' in cleanCotation or 'labande_ski_rating' in cleanCotation):
                output += ', Ski '
            elif currentCot < nbCots:
                output += ", "
                
            currentCot += 1
    return output

def format_cotation(cotationDict):
    if cotationDict is not None:
        return _format_cotation(cotationDict)

def format_activities(actList):
        if len(actList) != 0:
            return _format_activities(actList)

def _format_activities(actList):
    nbAct = len(actList)
    currentAct = 1
    output = ""
    
    for activite in actList:
        output += activites.get(activite, 'Undef.')
        if currentAct < nbAct:
            output += ", "
        currentAct += 1
    
    return output

def get_limitation(feuilleData):
    return int(feuilleData.getCellByPosition(1,1).Value)

def create_link(titleText, docID, lang='fr'):
    destURL = "https://www.camptocamp.org/routes/{}/{}".format(docID, lang)
    return('=HYPERLINK("{}";"{}")'.format(destURL, titleText))

def getC2CRouteList(*args):
    import json
    import requests

    ThisDoc = XSCRIPTCONTEXT.getDocument()
    feuilleData = ThisDoc.Sheets.getByName("Input")
    
    nbEltsToGet = get_limitation(feuilleData)
    if nbEltsToGet > limitElts:
        nbEltsToGet = limitElts
    
    # Get input URL & convert it to correct API URL
    # Position = (colonne, ligne) -> commencent à 0 !
    url = feuilleData.getCellByPosition(1,0).String
    url = url.replace('www', 'api')
    url = url.replace('%252C', '%2C')
    url = url.replace('#', '?')
    # Adding nbEltsToGet
    if nbEltsToGet > 0:
        url += "&limit={}".format(nbEltsToGet)
    feuilleData.getCellByPosition(1,2).String = url

    response = requests.get(url)
    data = json.loads(response.text)
    
    nbElts = data['total']
    
    feuilleData.getCellByPosition(1,3).Value = nbElts
    
    # Let's go onto second sheet
    shOut = ThisDoc.Sheets.getByName("List")
    ligne = 1 # La ligne 0 contient les headers
    
    for document in data['documents']:
        activities = document['activities']
        infos = document['locales'][0]
        docID = document['document_id']
        
        # Récup des infos sur la cotation
        cotation = get_cotation(document)
        orientations = get_orientation(document)
        
        titre = "{} : {}".format(infos['title_prefix'], infos['title'])
        
        
        routeInfo = format_cotation(cotation)
        orientations = format_orientation(orientations)
        diffHeight = get_diffHeight(document)
        if orientations is not None:
            routeInfo += ', {}'.format(orientations)
        if diffHeight is not None:
            routeInfo += ', {} m'.format(diffHeight)
        
        #shOut.getCellByPosition(0, ligne).String = titre
        # Now we create an autolink to the guidebook
        shOut.getCellByPosition(0, ligne).Formula = create_link(titre, docID, 'fr')
        shOut.getCellByPosition(1, ligne).String = format_activities(activities)
        shOut.getCellByPosition(2, ligne).String = routeInfo
        
        # In the end, don't forget to increase line by one…
        ligne += 1
    
g_exportedscripts=getC2CRouteList,
